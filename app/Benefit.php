<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Benefit extends Model
{
	 use Notifiable;
	 public static    function parent($service_id)
    {
        $slots = Benefit::where('parent_id', $service_id)->get();
        return $slots;
    }

}
