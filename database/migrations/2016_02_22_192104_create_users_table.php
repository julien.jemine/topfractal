<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('password');
<<<<<<< HEAD
            $table->string('first_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('type', 20)->nullable();
            $table->string('activation_code', 40)->nullable();
            $table->string('forgotten_password_code', 40)->nullable();
            $table->tinyInteger('active')->nullable();
            $table->integer('google_id')->nullable();
=======
<<<<<<< HEAD
            $table->string('first_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('type', 20)->nullable();
            $table->string('activation_code', 40)->nullable();
            $table->string('forgotten_password_code', 40)->nullable();
            $table->tinyInteger('active')->nullable();
            $table->integer('google_id')->nullable();
=======
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('type', 20);
            $table->string('activation_code', 40)->nullable();
            $table->string('forgotten_password_code', 40)->nullable();
            $table->tinyInteger('active');
>>>>>>> e4e93a857e6bec4519848c041739e048262d8989
>>>>>>> 96d3a404d5d77d001ce201e226028894ea254e98
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
