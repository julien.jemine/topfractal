@extends('layouts.master')

@section('title')
Dashboard | Domain
@endsection

@section('content')

@include('includes.nav-bar')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-9 col-sm-8">
			<h1><span class="fui-list"> {{Lang::get('messages.Domains')}}</h1>
		</div><!-- /.col -->
	</div><!-- /.row -->
	<hr class="dashed margin-bottom-30">
	<!-- <div class="row">
		<h3> {{Lang::get('messages.createBenefits')}}</h3>
		<div class="col-md-8 col-md-offset-2">
			<form method="POST" action="{{route('benefits.add', app()->getLocale())}}">
				@csrf
				<div class="form-group">
					
					<div class="col-md-6">
						<label for="username" class="col-md-3 control-label"> {{Lang::get('messages.name')}}:</label>
						<input type="text" class="form-control" id="name" name="desc" placeholder=" {{Lang::get('messages.name')}}" value="">
					
						<button  class="btn btn-primary margin-top-30"><span class="fui-check"></span> {{Lang::get('messages.create')}}</button>
					</div>
				</div>
					
			</form>
		</div>
	</div> -->
		

	<div class="row  margin-top-30">
		<div class="col-md-6 col-md-offset-3">
			<ul class="list-group">
				@foreach ($data as $value)
				<li  class="list-group-item"><span>{{Lang::get('messages.name')}}: {{$value->site_name}}</span> <span>{{Lang::get('messages.domain')}}:  {{$value->remote_url}}</span></li>
			@endforeach
			</ul>
		</div>
	</div>
	
</div><!-- /.container -->


<style type="text/css">
	.list-group-item {
    position: relative;
    display: flex;
    padding: 15px 15px;
    margin-bottom: -1px;
    background-color: #fff;
    border: 1px solid #ddd;
    justify-content: space-between;
    align-items: center;
}
</style>
<!-- /Modals -->

<!-- Load JS here for greater good =============================-->
<script src="{{ URL::to('src/js/vendor/jquery.min.js') }}"></script>
<script src="{{ URL::to('src/js/vendor/flat-ui-pro.min.js') }}"></script>
<script src="{{ URL::to('src/js/vendor/jquery.zoomer.js') }}"></script>
<script src="{{ URL::to('src/js/build/users.js') }}"></script>
	

@endsection