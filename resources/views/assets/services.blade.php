@extends('layouts.master')

@section('title')
Dashboard | Services
@endsection

@section('content')

@include('includes.nav-bar')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-9 col-sm-8">
			<h1><span class="fui-window"> {{Lang::get('messages.Services')}}</h1>
		</div><!-- /.col -->
	</div><!-- /.row -->
	<hr class="dashed margin-bottom-30">
	<div class="row">
		<h3> {{Lang::get('messages.createServices')}}</h3>
		<div class="col-md-8 col-md-offset-2">
			<form method="POST" action="{{route('services.add', app()->getLocale())}}">
				@csrf
				<div class="form-group">
					
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<label for="username" class="col-md-3 control-label">  {{Lang::get('messages.name')}}:</label>
								<input type="text" class="form-control" id="name" name="desc" placeholder=" {{Lang::get('messages.name')}}" value="">
							</div>
							<div class="col-md-6">
								<label for="parent_id" class="col-md-3 control-label">  {{Lang::get('messages.parent')}}:</label>
								<select  id="parent_id" name="parent_id" class="select2-container form-control select select-inverse btn-block mbl">
									<option value="0" selected> {{Lang::get('messages.no_parent')}}</option>
									@foreach ($services as $service)
										<option value="{{$service->id}}">{{$service->desc}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="row" style="margin-left: 0px">
							<button  class="btn btn-primary margin-top-30"><span class="fui-check"></span> {{Lang::get('messages.create')}}</button>
						</div>						
						
					</div>
				</div>
					
			</form>
		</div>
	</div>
		

	<div class="row  margin-top-30">
		<div class="col-md-8 col-md-offset-2">
			<ul class="list-group">
				@foreach ($services as $service)
				<li  class="list-group-item">
					<a onclick="myFunction( {{$service->id}} ) "  >{{$service->desc}}</a><a href="{{route( 'services-delete', ['locale'=>app()->getLocale(), 'service'=>$service->id] ) }}" class="btn btn-danger">X</a>					
				</li>
				<ul id="elementService{{$service->id}}" >
					
				</ul>
			@endforeach
			</ul>
		</div>
	</div>
	
</div><!-- /.container -->


<style type="text/css">
	a:hover {
 		cursor:pointer;
	}
	.list-group-item {
    position: relative;
    display: flex;
    padding: 15px 15px;
    margin-bottom: -1px;
    background-color: #fff;
    border: 1px solid #ddd;
    justify-content: space-between;
    align-items: center;
}
</style>

<script type="text/javascript">
	function myFunction (id) {
		
		$.ajax({
		    type: 'GET', 
		    url:  window.location.origin+'/en/services-parent/'+id,
		    error: function() { 
		         console.log(data);
		    }
		}).done(function(data){
			var listContainer = '';
			 data.reverse().forEach((data, i) => {
		           listContainer+=`<li  class="list-group-item">${data.desc} <a href="${window.location.origin}/en/services-delete/${data.id}" class="btn btn-danger">X</a></li>`					
		   		
	       })

            document.getElementById("elementService"+id).innerHTML=listContainer;
		})
	}
</script>
<!-- /Modals -->

<!-- Load JS here for greater good =============================-->
<script src="{{ URL::to('src/js/vendor/jquery.min.js') }}"></script>
<script src="{{ URL::to('src/js/vendor/flat-ui-pro.min.js') }}"></script>
<script src="{{ URL::to('src/js/vendor/jquery.zoomer.js') }}"></script>
<script src="{{ URL::to('src/js/build/users.js') }}"></script>
	

@endsection