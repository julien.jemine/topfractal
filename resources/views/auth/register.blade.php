@extends('layouts.main')

@section('content')

    <div id="titlebar" class="margin-bottom-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{Lang::get('messages.register')}}</h2>

                </div>
            </div>
        </div>
    </div>
    <div class="container">


        <div class="row margin-top-75 ">
            <!-- Contact Form -->
            <div class="col-md-8 col-md-offset-2">

                <form  method="POST" action="{{ route('register', app()->getLocale()) }}">
                    @csrf

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <label for="email" class="col-md-6 col-form-label text-md-right">E-Mail</label>

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-primary">
                                {{Lang::get('messages.registers')}}
                            </button>
                        </div>
                    </div>
                </form>

            </div>
            <!-- Contact Form / End -->

        </div>
        <div class="row margin-bottom-75">
            <div class="col-md-8 col-md-offset-2">
                <div class="row text-aligen-center">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="col-md-6 padding-bottom-50 padding-top-50 text-aligen-center">
                            <a class="btn btn-lg btn-primary btn-block" href="{{ url('auth/google') }}">
                                <i class="fa fa-google"></i> Google
                            </a>
                        </div>
                        <div class="col-md-6 padding-bottom-50 padding-top-50">
                            <a href="{{ route('facebook.redirect') }}" class="btn btn-primary"><i class="fa fa-facebook"></i> Facebook</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
