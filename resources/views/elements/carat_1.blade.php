<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Content</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/main.min.css" rel="stylesheet">
    <link href="./css/custom.min.css" rel="stylesheet">

    <link href="./css/style-headers.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="page" class="page">

    <div class="item content" id="content_section1">

        <div class="container">
            <header class="layout__head"> <!-- top panel -->
                <div class="tpan"> <div class="layout__center">
                    <div class="tpan__inner no-location">
                        <div class="tpan__item tpan__item_address">

                            <div class="editContent">{{isset($address) ? $address : 'м. Полтава, вул. Європейська, 110'}}
                            </div>
                        </div>
                        <div class="tpan__item tpan__item_tels tpan__item_text-bold">
                            <div class="editContent" >
                                <a href="tel:{{isset($phone) ? preg_replace('/[^0-9]/', '', $phone) : '(0532) 63-77-59'}}">{{isset($phone) ? $phone : '(0532) 63-77-59'}}</a>
                            </div>
                        </div>
                        <div class="tpan__right">
                            <div class="tpan__call js-open-modal editContent" data-modal="callback">
                                
                            </div>
                        </div>
                    </div>
                </div>
                </div> <!-- main menu -->
                <div class="lome"> <div class="layout__center">
                    <div class="lome__inner">
                        <div class="lome__logo ">
                            <a class="logo">
                                <div class="logo__icon-wrap">
                                    <img alt="" src="./images/icons/brush.svg" style="height: 45px;">

                                </div>
                                <p class="logo__title editContent">Стоматологія "{{isset($name) ? $name : 'Карат'}}"</p>
                                <p class="logo__text editContent">Дбайливе ставлення до <br> кожного пацієнта</p>
                            </a>
                        </div>
                        <div class="lome__menu">
                            <nav class="menu menu_main" id="menu">
                                <ul class="menu__list nav">
                                    <li class="menu__item editContent ">
                                        <a class="menu__link js-scrollTo" href="#about">Про клініку</a>
                                    </li>
                                    <li class="menu__item editContent ">
                                        <a class="menu__link js-scrollTo" href="#advantages">Переваги</a>
                                    </li>
                                    <li class="menu__item editContent ">
                                        <a class="menu__link js-scrollTo" href="#services">Послуги</a>
                                    </li>
                                    <li class="menu__item editContent">
                                        <a class="menu__link js-scrollTo" href="#gallery">Фото</a>
                                    </li>
                                    <li class="menu__item editContent">
                                        <a class="menu__link js-scrollTo" href="#reviews">Відгуки</a>
                                    </li>
                                    <li class="menu__item editContent">
                                        <a class="menu__link js-scrollTo" href="#app">Контакти</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="lome__menu-mobile js-open-modal" data-modal="menu">
                        </div>
                    </div>
                </div>
                </div> <!-- mobile menu -->
                <div class="memob">
                    <div class="memob__square js-open-modal" data-modal="menu">
                    </div>
                </div>
                <section class="offer offer_static" style="margin-top: 0px;">
                    <div class="offer__bgi-wrap">
                        <picture class="editContent">
                            <source srcset="images/offer__bgi-img-1.png" media="(min-width: 768px)" class="editContent">
                                <img class="offer__bgi-img" src="images/offer__img-mob-1.png" alt=""> </picture>
                        <picture>
                            <source srcset="images/offer__bgi-circles.png" media="(min-width: 768px)">
                            <img class="offer__bgi-circles" src="images/ni1px.png" alt="">
                        </picture>
                    </div> <div class="layout__center offer__layout-center">
                        <div class="offer__content">
                            <h1 class="title title_offer">
                                <span class="title__text editContent">Стоматологічна клініка<br> для всієї родини!</span>
                            </h1>
                            <div class="offer__words">
                                <div class="offer__text">
                                    <p class="editContent">{!! isset($text) ? $text : 'Ми надаємо повний комплекс високоякісних <br> стоматологічних послуг для всіх членів сім\'ї <br> за доступними цінами'!!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </header>
        </div>
    </div>
</div>
<!-- Load JS here for greater good =============================-->
    <script src="./js/build/build.min.js"></script>
</body>
</html>
