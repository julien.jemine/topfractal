<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Content</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/main.min.css" rel="stylesheet">
    <link href="./css/custom.min.css" rel="stylesheet">
    <link href="./css/prime.css" rel="stylesheet">

    <link href="./css/style-headers.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="page" class="page">

    <div class="item content" id="content_section26">

        <div class="container">
           <header>
        <div class="header__top">
            <div class="width-wrapper clearfix">
                <div class="header__left">
                   
                </div>
                <div class="header__right editContent">
                   {{isset($address) ? $address : 'м. Полтава, вул. Європейська, 110'}}
                            
                </div>
            </div>
        </div>

        <div class="header__bottom">
           
            <div class="width-wrapper">
                <div>
                    <button class="header__menu" id="menu-switch"><img src="./images/burger.svg" alt=""></button>
                    <a hreflang="ru" href="https://www.prime-med.ru/" class="header__logo"><img src="./images/logo.svg" alt="ПРАЙМ Стоматологические клиники"></a>
                    <button class="header__call" id="contacts-menu-switch"><img src="./images/call.svg" alt=""></button>
                    <div class="header__navigation">
                        
                                                <a hreflang="ru" href="#o-klinike">Про клініку</a>
                                                <a hreflang="ru" href="#uslugi">Ціни і послуги</a>
                        
                       
                    </div>
                    <div class="header__contacts">
                        <div>
                            <div class="header__contacts__phone">{{isset($phone) ? $phone : '(0532) 63-77-59'}}</div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <div class="header__hideable">
            
            </div>
        </div>
    </header>

    <main>
        <section class="tabbed-pages">

                            <input type="radio" name="tab_control" id="tab0" checked="">
            
                            <div class="tabbed-pages__page" data-tab-index="0" style="background-image: url(./images/image_001.png)">
                    <div class="tabbed-pages__big-text">Стоматологічна клініка
для всієї родини!</div>
                    <div class="tabbed-pages__text">{!! isset($text) ? $text : 'Ми надаємо повний комплекс високоякісних <br> стоматологічних послуг для всіх членів сім\'ї <br> за доступними цінами'!!}<</div>
                   
                </div>
                            
         

        </section>
        </div>
    </div>
</div>
<!-- Load JS here for greater good =============================-->
    <script src="./js/build/build.min.js"></script>
</body>
</html>
