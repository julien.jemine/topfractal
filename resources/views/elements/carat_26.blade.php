<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Content</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/main.min.css" rel="stylesheet">
    <link href="./css/custom.min.css" rel="stylesheet">
    <link href="./css/prime.css" rel="stylesheet">
    <link href="./css/materialize.min.css" rel="stylesheet">
   

    <link href="./css/style-headers.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <style type="text/css">
        .container {
            width: 100%;
        }
    </style>
<div id="page" class="page">

    <div class="item content" id="content_section1">

        <div class="container">          
             <section class="clinics-infogram">
                <div class="container">
                    <div class="row">
                                        <div class="col s12 m6 l3 xl2">
                                <img src="./images/icon-1.svg" alt="">
                                    <figcaption>
                                       
                                        <b>Довіра клієнтів</b>
                                    </figcaption>
                                </figure>
                                         </div>
                                        <div class="col s12 m6 l3 xl2">
                                <img src="./images/icon-2.svg" alt="">
                                    <figcaption>
                                        <b>Досвідченні лікарі</b>
                                      
                                    </figcaption>
                                </figure>    
                                </div>                                    
                                        <div class="col s12 m6 l3 xl2">
                                            <img src="./images/icon-4.svg" alt="">
                                                <figcaption>
                                                    <b>Постійне навчання</b>
                                                   
                                                </figcaption>
                                            </figure>
                                         </div>
                                        <div class="col s12 m6 l3 xl2">
                             
                                    <img src="./images/icon-5.svg" alt="">
                                    <figcaption>
                                        <b>Лабораторія</b>
                                        
                                    </figcaption>
                                </figure>
                                         </div>
                                        <div class="col s12 m6 l3 xl2">
                                <img src="./images/icon-6.svg" alt="">
                                    <figcaption>
                                        <b>Постійно відкриті</b>
                                    </figcaption>
                                </figure>
                                         </div>
                                    </div>
                </div>

            </section>

            <section class="clinics-text">
        <div class="container">
            <div class="row">
                <div class="col s12 m6 l8">
                    <h2>Про клініку</h2>
                        {!! isset($text) ? $text : 'Одним з основних досягнень клініки є здобута <strong>довіра наших пацієнтів</strong>, оскільки, велика кількість наших відвідувачів звертається до нас за рекомендаціями своїх рідних чи друзів.</p> <p>Успіх завдяки нашій кваліфікованій команді лікарів-стоматологів. Беручи до уваги інтереси наших пацієнтів, ми продовжуємо <strong>вдосконалювати наші знання</strong> та професійні якості: беремо участь у національних та міжнародних конференціях, курсах підвищення кваліфікації.</p> <p>Наша практика пропонує унікальний спектр стоматологічних послуг, які можуть допомогти Вам і Вашим дітям вести <strong>здоровий і комфортний спосіб життя.</strong>'!!}

            </div>
        </div>
    </section>
   
        </div>
        
    </div>
</div>
<!-- Load JS here for greater good =============================-->
    <script src="./js/build/build.min.js"></script>
</body>
</html>
