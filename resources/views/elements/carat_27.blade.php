<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Content</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/main.min.css" rel="stylesheet">
    <link href="./css/custom.min.css" rel="stylesheet">
    <link href="./css/prime.css" rel="stylesheet">
    <link href="./css/materialize.min.css" rel="stylesheet">
   

    <link href="./css/style-headers.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <style type="text/css">
        .container {
            width: 100%;
        }
    </style>
<div id="page" class="page">

    <div class="item content" id="content_section1">

        <div class="container">          
             <section class="clinics-infogram">
                <div class="container">
                    <div class="row services-and-prices">
                        <div class="col s12 m6 l6">
                            <h1>Послуги</h1>



                            <p>Ми допоможемо впоратися з БУДЬ-ЯКИМ стоматологічним недоліком, оскільки вирішуємо питання комплексно і запобігаємо появі нових проблем на ранньому етапі їхнього виникнення.</p>
                        </div>
                    </div>
                </div>

            </section>

            <section class="services">
                <div class="container">
                    
                    <div class="row">

                        @if(count($offers) > 0)
                                    @foreach($offers as $offer)
                                    <div class="col s12 m12 l6">
                                        <img alt="" class="services_img_hladii" src="{{$offer['img']}}" alt="" class="services__image">
                                         <div class="services__content">

                                            <p>{!!$offer['title']!!}</p>

                                          
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                        <div class="col s12 m12 l6">
                                 <img src="https://www.prime-med.ru/images/icons/services/2.svg" alt="" class="services__image">
                                <div class="services__content">
                                    <h3>Терапевтическая стоматология</h3>
                                    <p>Полноценные терапевтические отделения, позволяют нашим специалистам&nbsp;осуществлять полный объем терапевтической стоматологии:&nbsp;</p>

        <ul>
            <li>лечение кариеса и его осложнений;</li>
            <li>лечение пульпита и периодонтита;&nbsp;</li>
            <li>профилактика стоматологических заболеваний.</li>
        </ul>
                                </div>
                            </a>
                        </div>
                                        <div class="col s12 m12 l6">
                                 <img src="https://www.prime-med.ru/images/icons/services/1.svg" alt="" class="services__image">
                                <div class="services__content">
                                    <h3>Детская стоматология</h3>
                                    <ul>
            <li>&nbsp;грамотные профильные специалисты по стоматологии раннего возраста;</li>
            <li>&nbsp;специальное оборудование и материалы для маленьких пациентов;</li>
            <li>&nbsp;уютная детская атмосфера в кабинетах и холле детского отделения;</li>
            <li>&nbsp;аниматоры и мини-океанариум.</li>
        </ul>
                                </div>
                            </a>
                        </div>
                                        <div class="col s12 m12 l6">
                                 <img src="https://www.prime-med.ru/images/icons/services/3.svg" alt="" class="services__image">
                                <div class="services__content">
                                    <h3>Имплантация зубов в Рязани</h3>
                                    <ul>
            <li>комплексное обследование перед вживлением импланта;</li>
            <li>строгое соблюдение мер инфекционной безопасности;</li>
            <li>проверенные системы имплантации, от ведущих мировых производителей;</li>
            <li>большой клинический опыт - более 1800 поставленных имплантов ежегодно.</li>
        </ul>
                                </div>
                            </a>
                        </div>
                                        <div class="col s12 m12 l6">
                                 <img src="https://www.prime-med.ru/images/icons/services/5.svg" alt="" class="services__image">
                                <div class="services__content">
                                    <h3>Протезирование зубов в Рязани</h3>
                                    <ul>
            <li>протезирование зубов любой сложности - от полных съемных протезов до безметалловых коронок и мостов;</li>
            <li>протезирование на имплантах;</li>
            <li>собственная лицензированная зуботехническая лаборатория.</li>
        </ul>
                                </div>
                            </a>
                        </div>
                                        <div class="col s12 m12 l6">
                                 <img src="https://www.prime-med.ru/images/icons/services/17.svg" alt="" class="services__image">
                                <div class="services__content">
                                    <h3>Ортодонтия. Исправление прикуса в Рязани</h3>
                                    <p>На базе клиник функционирует специализи­рованный Центр Ортодонтии. В клиниках "Прайм-стоматология" прием ведут ведущие ортодонты Рязанской области, к.м.н.:</p>

        <ul>
            <li>самолигирующие брекеты;</li>
            <li>лингвальные брекеты;</li>
            <li>прозрачные капы.&nbsp;</li>
        </ul>
                                </div>
                            </a>
                        </div>
                                        <div class="col s12 m12 l6">
                                 <img src="https://www.prime-med.ru/images/icons/services/4.svg" alt="" class="services__image">
                                <div class="services__content">
                                    <h3>Отбеливание зубов</h3>
                                    <p>Специалисты «Прайм-стоматологии» проводят профессиональное отбеливание зубов</p>

        <p>ZOOM 4 White Speed</p>
                                </div>
                            </a>
                        </div>
                                        <div class="col s12 m12 l6">
                                 <img src="https://www.prime-med.ru/images/icons/services/14.svg" alt="" class="services__image">
                                <div class="services__content">
                                    <h3>Эстетическая стоматология</h3>
                                    <p>В клиниках "Прайм-стоматология" используются 2 метода эстетической реставрации зубов,&nbsp;позволяющие корректировать нарушения формы и цвета зуба:</p>

        <ul>
            <li>композитные виниры</li>
            <li>керамические виниры</li>
        </ul>
                                </div>
                            </a>
                        </div>
                                        <div class="col s12 m12 l6">
                                 <img src="https://www.prime-med.ru/images/icons/services/9.svg" alt="" class="services__image">
                                <div class="services__content">
                                    <h3>Пародонтология</h3>
                                    <p>Пародонтология&nbsp;- это профилактика и лечение заболеваний десен.</p>

        <ul>
            <li>получены патенты на способы хирургического лечения заболеваний пародонта;</li>
            <li>применяют 3 различных вида лазеров в пародонтологии;</li>
            <li>квалифицированные хирурги-пародонтологи, к.м.н.</li>
        </ul>
                                </div>
                            </a>
                        </div>
                                        <div class="col s12 m12 l6">
                                 <img src="https://www.prime-med.ru/images/icons/services/11.svg" alt="" class="services__image">
                                <div class="services__content">
                                    <h3>Рентгенология</h3>
                                    <p>Отделения рентгенодиагностики "Прайм-стоматологии" оснащены уникальным диагностическим оборудованием немецкой фирмы Sirona:</p>

        <ul>
            <li>радиовизиографом Heliodent</li>
            <li>дентальным томографом Orthophos 3D</li>
        </ul>



                                </div>
                            </a>
                        </div>
                                        <div class="col s12 m12 l6">
                                 <img src="https://www.prime-med.ru/images/icons/services/16.svg" alt="" class="services__image">
                                <div class="services__content">
                                    <h3>Лечение под микроскопом</h3>
                                    <ul>
            <li>диагностика заболеваний на ранних стадиях;</li>
            <li>сохранение своих зубов - возможность избежать удаления;</li>
            <li>лечение сложных зубов;</li>
            <li>комфорт для врача и пациента.</li>
        </ul>
                                </div>
                            </a>
                        </div>
                                        <div class="col s12 m12 l6">
                                 <img src="https://www.prime-med.ru/images/icons/services/6.svg" alt="" class="services__image">
                                <div class="services__content">
                                    <h3>Хирургическая стоматология</h3>
                                    <ul>
            <li>операции по удалению зубов любого уровня сложности;</li>
            <li>лечение воспалительных процессов;</li>
            <li>выдача больничного листа по показаниям пациенту.</li>
        </ul>
                                </div>
                            </a>
                        </div>
                            <div class="col s12 m12 l6">
                                 <img src="https://www.prime-med.ru/images/icons/services/100.svg" alt="" class="services__image">
                                <div class="services__content">
                                    <h3>Лечение под наркозом</h3>
                                    <ul>
                                        <li>лечение зубов под общим наркозом;</li>
                                        <li>лечение зубов с использованием закиси азота - седация</li>
                                    </ul>
                                </div>
                          
                        </div>
                           @endif
                                    </div>
                </div>
            </section>
   
        </div>
        
    </div>
</div>
<!-- Load JS here for greater good =============================-->
    <script src="./js/build/build.min.js"></script>
</body>
</html>
