<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Content</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/main.min.css" rel="stylesheet">
    <link href="./css/custom.min.css" rel="stylesheet">
    <link href="./css/prime.css" rel="stylesheet">
    <link href="./css/materialize.min.css" rel="stylesheet">
   

    <link href="./css/style-headers.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
   
<div id="page" class="page">

    <div class="item content" id="content_section1">

       
       <div class="container">
            <section class="app" id="app"><img class="app__bg-fig lazyautosizes lazyloaded" data-sizes="auto" alt="" src="images/app__bg-fig.png" sizes="1207px">
                <div class="layout__center">
                   
                    <div class="app__grid">
                        <div class="grid">
                            <div class="grid__row">
                                <div class="grid__col-lg-6">
                                    <div class="app__content"><h3 class="app__list-title editContent">Приходьте на консультацію,
                                        якщо Вас турбують:</h3>
                                         @if(isset($text))
                                        {!!$text!!}
                                        @else
                                        <div class="app__list editContent">
                                            <ul>
                                                <li>ниючий або гострий зубний біль;</li>
                                                <li>відламаний або пошкоджений зуб;</li>
                                                <li>кровоточивість ясен;</li>
                                                <li>кривизна зубів, їхня форма і колір;</li>
                                                <li>наліт, карієс і неприємний запах.</li>
                                            </ul>
                                        </div>
                                        @endif
                                        <div class="app__text editContent"><p>Ми допоможемо впоратися з БУДЬ-ЯКИМ стоматологічним
                                            недоліком, оскільки вирішуємо питання комплексно і запобігаємо появі нових
                                            проблем на ранньому етапі їхнього виникнення.</p>
                                            <p>&nbsp;<br>&nbsp;</p></div>
                                                                                
                                    </div>
                                </div>
                                <div class="grid__col-lg-6" id="app-form">
                                    <div class="app__form">
                                        <div class="app__ciround">
                                            <div class="ciround"><img class="ciround__img lazyautosizes lazyloaded" data-sizes="auto" alt="" src="images/ciround__img.jpg" sizes="818px">
                                                <div class="ciround__overlay"></div>
                                                <div class="ciround__circle"></div>
                                            </div>
                                        </div>
                                        <div class="app__content"><h3 class="app__list-title editContent">Нас легко знайти</h3>
                                        <div class="app__list editContent">
                                            <div class="contacts__item editContent">
                                            <div class="contacts__round"><img class="contacts__icon lazyautosizes lazyloaded" data-sizes="auto" alt="Адрес" src="images/location(1).svg" sizes="16px"></div>
                                            <div class="contacts__label editContent">Адреса:</div>
                                            <div class="contacts__text editContent">{{isset($address) ? $address : 'м. Полтава, вул. Європейська, 110'}}</div>
                                        </div>
                                        <div class="contacts__item editContent">
                                            <div class="contacts__round"><img class="contacts__icon lazyautosizes lazyloaded" data-sizes="auto" alt="График работы" src="images/clock.svg" sizes="16px"></div>
                                            <div class="contacts__label editContent">Графік роботи:</div>
                                            <div class="contacts__text editContent">Пн.-Пт.: 09:00-19:00, Сб.: 09:00-14:00, Нд.:
                                                вихідний
                                            </div>
                                        </div>
                                        <div class="contacts__item editContent">
                                            <div class="contacts__round"><img class="contacts__icon lazyautosizes lazyloaded" data-sizes="auto" alt="Телефон" src="images/call(1).svg" sizes="16px"></div>
                                            <div class="contacts__label editContent">Телефон:</div>
                                            <div class="contacts__text editContent">
                                                <a href="tel:{{isset($phone) ? $phone : '(0532) 63-77-59'}}">{{isset($phone) ? $phone : '(0532) 63-77-59'}}</a>
                                            </div>
                                        </div>
                                        <div class="contacts__item editContent">
                                            <div class="contacts__round"><img class="contacts__icon lazyautosizes lazyloaded" data-sizes="auto" alt="Электронная почта" src="images/mail.svg" sizes="16px"></div>
                                            <div class="contacts__label editContent">Електронна пошта:</div>
                                            <div class="contacts__text editContent">{{isset($email) ? $email : 'karat.dental@gmail.com'}}
                                            </div>
                                        </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></section>
        </div>
        
    </div>
</div>
<!-- Load JS here for greater good =============================-->
    <script src="./js/build/build.min.js"></script>
</body>
</html>
