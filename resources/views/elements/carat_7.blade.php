<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Content</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./css/main.min.css" rel="stylesheet">
    <link href="./css/custom.min.css" rel="stylesheet">

    <link href="./css/style-headers.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="page" class="page">

    <div class="item content" id="content_section1">

        <div class="container">
            <section class="reviews reviews-alt" id="reviews">
                <div class="layout__center"><h2 class="title text-center"><span class="title__text editContent">Наші пацієнти рекомендуют нас</span>
                </h2>
                    <div class="reviews__swiper">
                        <div class="reviews__swiper-controls">
                            <div
                                class="control control_left reviews__control reviews__control_left js-swiper-reviews-move-left"
                                tabindex="0" role="button" aria-label="Previous slide">
                                <div class="control__round">
                                    <div class="control__inner">
                                        <div class="control__icon"></div>
                                    </div>
                                </div>
                            </div>
                            <div
                                class="control control_right reviews__control reviews__control_right js-swiper-reviews-move-right"
                                tabindex="0" role="button" aria-label="Next slide">
                                <div class="control__round">
                                    <div class="control__inner">
                                        <div class="control__icon"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div
                            class="reviews__swiper-pagination js-swiper-reviews-pagination swiper-pagination-clickable swiper-pagination-bullets">
                            <span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0"
                                  role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet"
                                                                                        tabindex="0" role="button"
                                                                                        aria-label="Go to slide 2"></span><span
                            class="swiper-pagination-bullet" tabindex="0" role="button"
                            aria-label="Go to slide 3"></span></div>
                        <div class="swiper-container js-swiper-container-reviews swiper-container-horizontal"
                             style="cursor: grab;">
                            <div class="swiper-wrapper"
                                 style="transition-duration: 0ms; transform: translate3d(-1220px, 0px, 0px);">
                                <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next"
                                     data-swiper-slide-index="1" style="width: 530px; margin-right: 80px;">
                                    <div class="reviews__head">
                                        <div class="reviews__img-wrap">
                                            <div class="corner corner__left-top"></div>
                                            <div class="corner corner__right-bottom"></div>
                                            <img class="reviews__img swiper-lazy swiper-lazy-loaded" alt=""
                                                 src="images/reviews__img2.jpg">
                                        </div>
                                        <div class="reviews__figcaption">
                                            <div class="reviews__name editContent">Бондаренко Рината</div>
                                            <div class="reviews__date editContent">08.07.2019</div>
                                        </div>
                                    </div>
                                    <div class="reviews__words">
                                        <div class="reviews__text editContent"><p>Клініка невелика, але затишна і душевна. Ціни
                                            доступні, все стерильно. Я лікуюсь у лікара-ортодонта. Від неї віє
                                            впевненістю, знанням справи, майстерністю. Не відчуваю сумнівів в якості
                                            роботи. Вибрала стоматолога за рекомендацією і дуже задоволена.&nbsp;Мені 27
                                            років. Я виправляю зуби брекет-системою. Вже через місяць побачила
                                            результат. Проміжки зтягнулись, зуби стали в рівну лінію. На лікуванні вже
                                            півроку, всі зуби стали на місце(як на мене), та лікар доводе до ідеалу.
                                            Шкодую, що раніше не звернулася. Велике спасибі!</p></div>
                                    </div>
                                </div>

                                <div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0"
                                     style="width: 530px; margin-right: 80px;">
                                    <div class="reviews__head">
                                        <div class="reviews__img-wrap">
                                            <div class="corner corner__left-top"></div>
                                            <div class="corner corner__right-bottom"></div>
                                            <img class="reviews__img swiper-lazy swiper-lazy-loaded" alt=""
                                                 src="images/reviews__img1.jpg">
                                        </div>
                                        <div class="reviews__figcaption">
                                            <div class="reviews__name editContent">Вац Ігор Євгенович</div>
                                            <div class="reviews__date editContent">10.07.2019</div>
                                        </div>
                                    </div>
                                    <div class="reviews__words">
                                        <div class="reviews__text editContent"><p>Моє перше знайомство з клінікою «Карат» відбулося
                                            у травні, 2015 року. Дуже довго обирати клініку, де буду лікувати власні
                                            зуби, не довелося. За порадою знайомого звернувся у клініку «Карат». Мені
                                            сподобалася лікар — стоматолог Аліна Іваненко. Професіонал своєї справи і
                                            просто приємна і комунікабельна жінка. Ціна не кусається, та й в цілому
                                            обстановка налаштовує на лікування! Я хотів би ще раз подякувати Вам, Аліна
                                            Олександрівна, за порятунок моїх зубів. Ви повернули мене до повноцінного
                                            життя. Я можу рекомендувати цю клініку друзям.</p></div>
                                    </div>
                                </div>
                                <div class="swiper-slide swiper-slide-next" data-swiper-slide-index="1"
                                     style="width: 530px; margin-right: 80px;">
                                    <div class="reviews__head">
                                        <div class="reviews__img-wrap">
                                            <div class="corner corner__left-top"></div>
                                            <div class="corner corner__right-bottom"></div>
                                            <img class="reviews__img swiper-lazy swiper-lazy-loaded" alt=""
                                                 src="images/reviews__img2.jpg">
                                        </div>
                                        <div class="reviews__figcaption">
                                            <div class="reviews__name editContent">Бондаренко Рината</div>
                                            <div class="reviews__date editContent">08.07.2019</div>
                                        </div>
                                    </div>
                                    <div class="reviews__words">
                                        <div class="reviews__text editContent"><p>Клініка невелика, але затишна і душевна. Ціни
                                            доступні, все стерильно. Я лікуюсь у лікара-ортодонта. Від неї віє
                                            впевненістю, знанням справи, майстерністю. Не відчуваю сумнівів в якості
                                            роботи. Вибрала стоматолога за рекомендацією і дуже задоволена.&nbsp;Мені 27
                                            років. Я виправляю зуби брекет-системою. Вже через місяць побачила
                                            результат. Проміжки зтягнулись, зуби стали в рівну лінію. На лікуванні вже
                                            півроку, всі зуби стали на місце(як на мене), та лікар доводе до ідеалу.
                                            Шкодую, що раніше не звернулася. Велике спасибі!</p></div>
                                    </div>
                                </div>

                                <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active"
                                     data-swiper-slide-index="0" style="width: 530px; margin-right: 80px;">
                                    <div class="reviews__head">
                                        <div class="reviews__img-wrap">
                                            <div class="corner corner__left-top"></div>
                                            <div class="corner corner__right-bottom"></div>
                                            <img class="reviews__img swiper-lazy swiper-lazy-loaded" alt=""
                                                 src="images/reviews__img1.jpg">
                                        </div>
                                        <div class="reviews__figcaption">
                                            <div class="reviews__name editContent">Вац Ігор Євгенович</div>
                                            <div class="reviews__date editContent">10.07.2019</div>
                                        </div>
                                    </div>
                                    <div class="reviews__words">
                                        <div class="reviews__text editContent"><p>Моє перше знайомство з клінікою «Карат» відбулося
                                            у травні, 2015 року. Дуже довго обирати клініку, де буду лікувати власні
                                            зуби, не довелося. За порадою знайомого звернувся у клініку «Карат». Мені
                                            сподобалася лікар — стоматолог Аліна Іваненко. Професіонал своєї справи і
                                            просто приємна і комунікабельна жінка. Ціна не кусається, та й в цілому
                                            обстановка налаштовує на лікування! Я хотів би ще раз подякувати Вам, Аліна
                                            Олександрівна, за порятунок моїх зубів. Ви повернули мене до повноцінного
                                            життя. Я можу рекомендувати цю клініку друзям.</p></div>
                                    </div>
                                </div>
                                <div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-next"
                                     data-swiper-slide-index="1" style="width: 530px; margin-right: 80px;">
                                    <div class="reviews__head">
                                        <div class="reviews__img-wrap">
                                            <div class="corner corner__left-top"></div>
                                            <div class="corner corner__right-bottom"></div>
                                            <div class="swiper-lazy-preloader"></div>
                                            <img class="reviews__img swiper-lazy"
                                                 data-src="https://karat-dental.com.ua/wp-content/uploads/sites/49/reviews__img2.jpg"
                                                 alt=""
                                                 src="images/team__src-lazy-img.jpg">
                                        </div>
                                        <div class="reviews__figcaption">
                                            <div class="reviews__name editContent">Бондаренко Рината</div>
                                            <div class="reviews__date editContent">08.07.2019</div>
                                        </div>
                                    </div>
                                    <div class="reviews__words">
                                        <div class="reviews__text editContent"><p>Клініка невелика, але затишна і душевна. Ціни
                                            доступні, все стерильно. Я лікуюсь у лікара-ортодонта. Від неї віє
                                            впевненістю, знанням справи, майстерністю. Не відчуваю сумнівів в якості
                                            роботи. Вибрала стоматолога за рекомендацією і дуже задоволена.&nbsp;Мені 27
                                            років. Я виправляю зуби брекет-системою. Вже через місяць побачила
                                            результат. Проміжки зтягнулись, зуби стали в рівну лінію. На лікуванні вже
                                            півроку, всі зуби стали на місце(як на мене), та лікар доводе до ідеалу.
                                            Шкодую, що раніше не звернулася. Велике спасибі!</p></div>
                                    </div>
                                </div>
                            </div>
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
                    </div>
                </div>
            </section> <!-- price -->

        </div>
    </div>
</div>
<!-- Load JS here for greater good =============================-->
<script src="./js/bundle.min.js" defer="defer"></script>
<script type="text/javascript" src="./js/jquery.js"></script>


<script type="text/javascript" src="./js/scripts.js"></script>
<script src="./js/build/build.min.js"></script>
<script src="./js/build/build.min.js"></script>
</body>
</html>
