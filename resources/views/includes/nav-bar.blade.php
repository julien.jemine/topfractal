<nav class="mainnav navbar navbar-inverse navbar-embossed navbar-fixed-top" role="navigation" id="mainNav">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
			<span class="sr-only">Toggle Navigation</span>
		</button>
		<a class="navbar-brand" href="{{route('home')}}">
			<b class='text-primary'>DENT</b>ECO
		</a>
	</div>
	<div class="collapse navbar-collapse" id="navbar-collapse-01">
		<ul class="nav navbar-nav">
			@if (isset($data['siteData']) || (isset($page) && $page == 'newPage'))
			@if (isset($data['siteData']))
			<li class="active">
				<a><span class="fui-home"></span> <span id="siteTitle">{{ $data['siteData']['site'][0]->site_name }}</span></a>
			</li>
			@endif
			@if (isset($page) && $page == 'newPage')
			<li class="active">
				<a><span class="fui-home"></span> <span id="siteTitle">{{Lang::get('messages.My_New_Site')}}</span> </a>
			</li>
			@endif
			<li><a href="../dashboard" id="backButton"><span class="fui-arrow-left"></span> {{Lang::get('messages.Back_to_Sites')}}</a></li>
			@else
			<li class="{{ Request::path() ==  '../dashboard' ? 'active' : ''  }}"><a href="{{ route('dashboard', app()->getLocale()) }}"><span class="fui-windows"></span> {{Lang::get('messages.Sites')}}</a></li>
			<li class="{{ Request::path() ==  'assets' ? 'active' : ''  }}"><a href="{{ route('assets', app()->getLocale()) }}"><span class="fui-image"></span> {{Lang::get('messages.Image_Library')}}</a></li>
			@if (Auth::user()->type == 'admin')
			<li class="{{ Request::path() ==  'users' ? 'active' : ''  }}"><a href="{{ route('users', app()->getLocale()) }}"><span class="fui-user"></span> {{Lang::get('messages.Users')}}</a></li>
			<li class="{{ Request::path() ==  'benefits' ? 'active' : ''  }}"><a href="{{ route('benefits', app()->getLocale()) }}"><span class="fui-window"></span> {{Lang::get('messages.Benefits')}}</a></li>
			<li class="{{ Request::path() ==  'services' ? 'active' : ''  }}"><a href="{{ route('services', app()->getLocale()) }}"><span class="fui-cmd"></span> {{Lang::get('messages.Services')}}</a></li>
			<li class="{{ Request::path() ==  'domains' ? 'active' : ''  }}"><a href="{{ route('domains', app()->getLocale()) }}"><span class="fui-list"></span> {{Lang::get('messages.Domains')}}</a></li>
			@endif
			@endif
		</ul>
		<ul class="nav navbar-nav navbar-right" style="margin-right: 20px;">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{Lang::get('messages.Hi')}}, {{ Auth::user()->first_name }} <b class="caret"></b></a>
				<span class="dropdown-arrow"></span>
				<ul class="dropdown-menu">
					<li><a href="#accountModal" data-toggle="modal"><span class="fui-cmd"></span> {{Lang::get('messages.My_Account')}}</a></li>
					<li class="divider"></li>
					<li><a class="dropdown-item" href="{{ route('logout', app()->getLocale()) }}"
                           onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                            <i class="sl sl-icon-power"></i> {{Lang::get('messages.Logout')}}
                        </a>

                        <form id="logout-form" action="{{ route('logout', app()->getLocale()) }}" method="POST" style="display: none;">
                            @csrf
                        </form></li>
				</ul>
			</li>
		</ul>
	</div><!-- /.navbar-collapse -->
</nav><!-- /navbar -->
