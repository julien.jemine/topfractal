<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<base href=" {{ url('/') }}">
	<!-- Basic Page Needs
    ================================================== -->
	<title>{{ config('app.name', 'DentEco') }}</title>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
	

	<!-- CSS
    ================================================== -->


	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/main-color.css" id="colors">
	<link rel="stylesheet" href="{{ asset('css/bootstrap-tagsinput.css') }}" />
	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
	<script type="text/javascript" src="{{ asset('scripts/jquery-3.4.1.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('scripts/jquery-migrate-3.1.0.min.js')}}"></script>
	<!-- Styles -->

</head>

<body>

<!-- Wrapper -->
<div id="wrapper">


	<!-- Header Container
    ================================================== -->
	<header id="header-container">

		<!-- Header -->
		<div id="header">
			<div class="container">

				<!-- Left Side Content -->
				<div class="left-side">

					<!-- Logo -->
					<div id="logo">
						<a href="{{route('home', app()->getLocale())}}"><img src="images/logo.png" alt=""></a>
					</div>

					<!-- Mobile Navigation -->
					<div class="mmenu-trigger">
						<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
						</button>
					</div>
					<nav id="navigation" class="style-1">
						<ul id="responsive">
							<li><a href="{{route('oferta', app()->getLocale())}}">{{Lang::get('messages.public')}}</a>
							</li>
                            <li><a href="{{route('polityka.konfidentsijnosti', app()->getLocale())}}">{{Lang::get('messages.polityka')}}</a>
                            </li>
                            @guest
                            @else
                                <li> <a class="sign-in" href="{{ route('dashboard', app()->getLocale()) }}">{{Lang::get('messages.dashboard')}}</a></li>

                            @endguest


							<li>

								@foreach (config('app.available_locales') as $locale)

										<a href="{{ route(\Route::currentRouteName(), [$locale, $page->id ?? '']) }}"
											@if (app()->getLocale() == $locale) style="display:none" @endif>{{ strtoupper($locale) }}</a>

								@endforeach

							</li>


						</ul>
					</nav>
				</div>
				<!-- Left Side Content / End -->


				<!-- Right Side Content / End -->

				<div class="right-side">
                    <div class="header-widget">
                        @guest
                             <a class="sign-in" href="{{ route('register', app()->getLocale()) }}"> {{Lang::get('messages.register')}}</a>
                             <span class="sign-in">|</span>
                             <a class="sign-in" href="{{ route('login', app()->getLocale()) }}"> {{Lang::get('messages.login')}}</a>

                        @else
                            <a class="sign-in"  href="{{ route('logout', app()->getLocale()) }}"
                                   onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                    <i class="sl sl-icon-power"></i> {{Lang::get('messages.logout')}}
                                </a>

                                <form id="logout-form" action="{{ route('logout', app()->getLocale()) }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                             <a href="{{ route('layouts', app()->getLocale()) }}" class="button border with-icon"> {{Lang::get('messages.create')}} <i class="sl sl-icon-plus"></i></a>
                        @endguest
					</div>
				</div>

				<!-- Right Side Content / End -->

			</div>
		</div>
		<!-- Header / End -->

	</header>
	<div class="clearfix"></div>
	<!-- Header Container / End -->


@yield('content')

<!-- Footer
================================================== -->
	<div id="footer" class="sticky-footer">
		<!-- Main -->
		<div class="container">


			<!-- Copyright -->
			<div class="row">


					<div class="copyrights">{{ Lang::get('messages.all_rights_reserved') }}</div>

				</div>
			</div>

		</div>

	</div>
	<!-- Footer / End -->


	<!-- Back To Top Button -->
	<div id="backtotop"><a href="#"></a></div>

</div>
<!-- Wrapper / End -->


<!-- Scripts
================================================== -->


<script type="text/javascript" src="{{ asset('scripts/mmenu.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('scripts/chosen.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('scripts/slick.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('scripts/rangeslider.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('scripts/magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('scripts/waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('scripts/counterup.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('scripts/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('scripts/tooltips.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('scripts/custom.js')}}"></script>
<script type="text/javascript" src="{{ asset('scripts/bootstrap-tagsinput.js')}}"></script>

<!-- Booking Widget - Quantity Buttons -->
{{--<script src="{{ asset('scripts/quantityButtons.js')}}"></script>--}}

<!-- Date Range Picker - docs: http://www.daterangepicker.com/ -->
<script src="{{ asset('scripts/moment.min.js')}}"></script>
<script src="{{ asset('scripts/daterangepicker.js')}}"></script>

<script type="text/javascript" src="{{ asset('scripts/DateTimePicker/build/jquery.datetimepicker.full.min.js')}}"></script>

<!-- Leaflet // Docs: https://leafletjs.com/ -->
<script src="{{ asset('scripts/leaflet.min.js')}}"></script>

<!-- Leaflet Maps Scripts -->
<script src="{{ asset('scripts/leaflet-markercluster.min.js')}}"></script>
<script src="{{ asset('scripts/leaflet-gesture-handling.min.js')}}"></script>
<script src="{{ asset('scripts/leaflet-listeo.js')}}"></script>

<!-- Leaflet Geocoder + Search Autocomplete // Docs: https://github.com/perliedman/leaflet-control-geocoder -->

<script src="{{ asset('scripts/leaflet-autocomplete.js')}}"></script>
<script src="{{ asset('scripts/leaflet-control-geocoder.js')}}"></script>

<script type="text/javascript" src="{{ asset('scripts/dropzone.js')}}"></script>

<!-- Typed Script -->
<script type="text/javascript" src="{{ asset('scripts/typed.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>



<!-- Style Switcher
================================================== -->
<script src="{{ asset('scripts/switcher.js')}}"></script>

<div id="style-switcher">
	<h2>Color Switcher <a href="#"><i class="sl sl-icon-settings"></i></a></h2>

	<div>
		<ul class="colors" id="color1">
			<li><a href="#" class="main" title="Main"></a></li>
			<li><a href="#" class="blue" title="Blue"></a></li>
			<li><a href="#" class="green" title="Green"></a></li>
			<li><a href="#" class="orange" title="Orange"></a></li>
			<li><a href="#" class="navy" title="Navy"></a></li>
			<li><a href="#" class="yellow" title="Yellow"></a></li>
			<li><a href="#" class="peach" title="Peach"></a></li>
			<li><a href="#" class="beige" title="Beige"></a></li>
			<li><a href="#" class="purple" title="Purple"></a></li>
			<li><a href="#" class="celadon" title="Celadon"></a></li>
			<li><a href="#" class="red" title="Red"></a></li>
			<li><a href="#" class="brown" title="Brown"></a></li>
			<li><a href="#" class="cherry" title="Cherry"></a></li>
			<li><a href="#" class="cyan" title="Cyan"></a></li>
			<li><a href="#" class="gray" title="Gray"></a></li>
			<li><a href="#" class="olive" title="Olive"></a></li>
		</ul>
	</div>

</div>
<!-- Style Switcher / End -->


</body>
</html>
