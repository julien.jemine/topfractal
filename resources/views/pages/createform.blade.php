@extends('layouts.main')

@section('content')
     
    <script type="text/javascript" src="https://dent.eco/scripts/jquery.ui.touch.js"></script>



<script type="text/javascript">
   
</script>

      <div class="dashboard-content" style="margin-left:0px !important">

   
     
            <!-- Headline -->
            <div class="add-listing-headline">
                <h3><i class="sl sl-icon-book-open"></i>{{ Lang::get('messages.Make_your_site') }}</h3>
                <!-- Switcher -->
            </div>

            <form action="{{route('site-create-form', ['locale' => app()->getLocale(), 'page' => $page->id])}}" method="POST" enctype="multipart/form-data">

                @csrf
                <div class="row ">
                    <input type="hidden" name="page_id" id="page_id" value="{{$page->id}}">
                    <input type="hidden" name="locale" id="locale" value="{{app()->getLocale()}}">
                    <div class="col-lg-6 col-md-offset-3">
                        <!-- One "tab" for each step in the form: -->
                        <div class="tab">{{ Lang::get('messages.Main') }}:
                            <h5>{{ Lang::get('messages.Name_of_the_clinic') }} <i class="tip" data-tip-content="{{ Lang::get('messages.Name_of_the_clinic_message') }}"><div class="tip-content">{{ Lang::get('messages.Name_of_the_clinic_message') }}</div></i></h5>
                            <input name="site_name" class="search-field"   type="text" value="" >
                            
                        </div>

                        <div class="tab">{{ Lang::get('messages.Blocks') }}:
                            
                            <div id="tamplate_header">
                                
                            </div>
                            <ul id="elementCats"  class="form-check">
                              
                            </ul>
                             <div id="tamplate_footer">
                                
                            </div>

                        </div>
                        <div style="overflow:auto;">
                            <div style="float:right;">
                                <button type="button" id="prevBtn" onclick="nextPrev(-1)">{{ Lang::get('messages.Back') }}</button>
                                <button type="button" id="nextBtn" onclick="nextPrev(1)">{{ Lang::get('messages.Next') }}</button>
                                <button class="button preview" style="display: none;" id = "submitBtn" type="submit">{{ Lang::get('messages.Create') }}</button>
                            </div>
                        </div>

                        <!-- Circles which indicates the steps of the form: -->
                        <div style="text-align:center;margin-top:40px;">
                            <span class="step"></span>
                            <span class="step"></span>
                        </div>
                    </div>
                </div>
            </form>


        </div>
        <!-- Material checked -->



@endsection
