@extends('layouts.main')

@section('content')

    <div id="myCarousel" class="carousel slide " data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="images/person-writing-on-notebook-4145190.jpg" >

                <div class="carousel-caption">
                    <h2>{{Lang::get('messages.skills')}}</h2>
                    <h4>{{Lang::get('messages.learn')}}</h4>
                </div>
            </div>
            <div class="item">
                <img src="images/Canva - Cheerful surprised woman sitting with laptop.jpg" >

                <div class="carousel-caption">
                    <h2>{{Lang::get('messages.skills')}}</h2>
                    <h4>{{Lang::get('messages.learn')}}</h4>
                </div>
            </div>
            <div class="item">
                <img src="images/Canva - Photo of Boy Video Calling With a Woman.jpg" >

                <div class="carousel-caption">
                    <h2>{{Lang::get('messages.skills')}}</h2>
                    <h4>{{Lang::get('messages.learn')}}</h4>
                </div>
            </div>
            <div class="item">
                <img src="images/Canva - Woman Having A Video Call.jpg" >

                <div class="carousel-caption">
                    <h2>{{Lang::get('messages.skills')}} </h2>
                    <h4>{{Lang::get('messages.learn')}}</h4>
                </div>
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>


        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>

        </a>

    </div>

<!-- Content
================================================== -->

<section class="fullwidth padding-top-75 " data-background-color="#fff">
    <div class="container">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3 class="headline centered headline-extra-spacing">
                <strong class="headline-with-separator">{{Lang::get('messages.templates')}}</strong>
               </h3>
        </div>
    </div>


    <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="simple-slick-carousel dots-nav">

                    <!-- Listing Item -->
                    <div class="carousel-item">

                            <div class="listing-item">
                                <img src="elements/images/temaplte1.png" alt="">
                                <div class="listing-item-content">
                                    @if(\Illuminate\Support\Facades\Auth::check())
                                    <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'83']) }}"><span class="tag">.
                                    @else
                                    <a href="{{ route('login', ['locale'=>app()->getLocale()]) }}"><span class="tag">   
                                    @endif
                                    {{Lang::get('messages.demo')}}</span></a>
                                    <h3>{{Lang::get('messages.dentists')}}<i class="verified-icon"></i></h3>
                                <span>{{Lang::get('messages.universal')}}</span>
                                </div>
                            </div>
                            <div class="star-rating-hladii">

                                <span class="total_cost">1200 ₴</span>
                                @if(\Illuminate\Support\Facades\Auth::check())
                                <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'83']) }}" class="button book-now">
                                    {{Lang::get('messages.select')}}
                                </a>
                                @else
                                 <a href="{{route('login', ['locale'=>app()->getLocale()])}}" class="button book-now">
                                    {{Lang::get('messages.select')}}
                                </a>
                                @endif

                            </div>

                    </div>
                    <div class="carousel-item">

                            <div class="listing-item">
                                <img src="elements/images/prime1.png" alt="">
                                <div class="listing-item-content">
                                    @if(\Illuminate\Support\Facades\Auth::check())
                                    <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'302']) }}"><span class="tag">.
                                    @else
                                    <a href="{{ route('login', ['locale'=>app()->getLocale()]) }}"><span class="tag">   
                                    @endif
                                {{Lang::get('messages.demo')}}</span></a>
                                    <h3>{{Lang::get('messages.dentists')}}<i class="verified-icon"></i></h3>
                                <span>{{Lang::get('messages.universal')}}</span>
                                </div>
                            </div>
                            <div class="star-rating-hladii">

                                <span class="total_cost">1200 ₴</span>
                               @if(\Illuminate\Support\Facades\Auth::check())
                                <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'302']) }}" class="button book-now">
                                    {{Lang::get('messages.select')}}
                                </a>
                                @else
                                 <a href="{{route('login', ['locale'=>app()->getLocale()])}}" class="button book-now">
                                    {{Lang::get('messages.select')}}
                                </a>
                                @endif{{Lang::get('messages.select')}}</a>

                            </div>

                    </div>
                </div>

        </div>

</div>
</section>

<section class="fullwidth padding-top-75" data-background-color="#fff">
	<!-- Info Section -->
	<div class="container">

        <div class="row">
            <div class="col-md-12">
                <h3 class="headline centered ">
                    <strong class="headline-with-separator">{{Lang::get('messages.reviews')}}</strong>

                </h3>
            </div>
        </div>

	</div>
	<!-- Info Section / End -->

	<!-- Categories Carousel -->
	<div class="fullwidth-carousel-container margin-top-20 no-dots">
		<div class="testimonial-carousel testimonials">

			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial">{{Lang::get('messages.reviews1')}}</div>
				</div>
				<div class="testimonial-author">

					<h4>{{ Lang::get('messages.reviews_name_1') }}</h4>

				</div>
			</div>

			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial">{{Lang::get('messages.reviews2')}}</div>
                </div>
				<div class="testimonial-author">


					<h4>{{ Lang::get('messages.reviews_name_2') }}</h4>

				</div>
			</div>

			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial">{{Lang::get('messages.reviews3')}}</div>
                </div>
				<div class="testimonial-author">


					<h4>{{ Lang::get('messages.reviews_name_3') }}</h4>

				</div>
			</div>

		</div>
	</div>
	<!-- Categories Carousel / End -->

</section>

<section class="fullwidth padding-top-75 padding-bottom-30" data-background-color="#fff">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h3 class="headline centered">
                    <strong class="headline-with-separator">{{Lang::get('messages.about')}}</strong>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- Headline -->
                {{Lang::get('messages.about_text')}}

            </div>
        </div>

    </div>
</section>


<!-- Info Section -->

<!-- Info Section / End -->


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

@endsection
