@extends('layouts.main')

@section('content')

<!-- Content
================================================== -->
<div id="titlebar" class="gradient">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h2>{{Lang::get('messages.choose_templates')}}</h2><span></span>               

            </div>
        </div>
    </div>
</div>

<section class="fullwidth padding-top-20" data-background-color="#fff">
    <div class="container">

       <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="simple-slick-carousel dots-nav">

                    <!-- Listing Item -->
                    <div class="carousel-item">

                            <div class="listing-item">
                                <img src="elements/images/temaplte1.png" alt="">
                                <div class="listing-item-content">
                                    @if(\Illuminate\Support\Facades\Auth::check())
                                    <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'83']) }}"><span class="tag">.
                                    @else
                                    <a href="{{ route('login', ['locale'=>app()->getLocale()]) }}"><span class="tag">   
                                    @endif
                                    {{Lang::get('messages.demo')}}</span></a>
                                    <h3>{{Lang::get('messages.dentists')}}<i class="verified-icon"></i></h3>
                                <span>{{Lang::get('messages.universal')}}</span>
                                </div>
                            </div>
                            <div class="star-rating-hladii">

                                <span class="total_cost">1200 ₴</span>
                                @if(\Illuminate\Support\Facades\Auth::check())
                                <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'83']) }}" class="button book-now">
                                    {{Lang::get('messages.select')}}
                                </a>
                                @else
                                 <a href="{{route('login', ['locale'=>app()->getLocale()])}}" class="button book-now">
                                    {{Lang::get('messages.select')}}
                                </a>
                                @endif

                            </div>

                    </div>
                    <div class="carousel-item">

                            <div class="listing-item">
                                <img src="elements/images/prime1.png" alt="">
                                <div class="listing-item-content">
                                    @if(\Illuminate\Support\Facades\Auth::check())
                                    <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'302']) }}"><span class="tag">.
                                    @else
                                    <a href="{{ route('login', ['locale'=>app()->getLocale()]) }}"><span class="tag">   
                                    @endif
                                {{Lang::get('messages.demo')}}</span></a>
                                    <h3>{{Lang::get('messages.dentists')}}<i class="verified-icon"></i></h3>
                                <span>{{Lang::get('messages.universal')}}</span>
                                </div>
                            </div>
                            <div class="star-rating-hladii">

                                <span class="total_cost">1200 ₴</span>
                               @if(\Illuminate\Support\Facades\Auth::check())
                                <a href="{{ route('site-create', ['locale'=>app()->getLocale(), 'page'=>'302']) }}" class="button book-now">
                                    {{Lang::get('messages.select')}}
                                </a>
                                @else
                                 <a href="{{route('login', ['locale'=>app()->getLocale()])}}" class="button book-now">
                                    {{Lang::get('messages.select')}}
                                </a>
                                @endif{{Lang::get('messages.select')}}</a>

                            </div>

                    </div>
                </div>

        </div>
        <!-- Row / End -->

    </div>
</section>
<!-- Info Section -->

<!-- Info Section / End -->



@endsection
