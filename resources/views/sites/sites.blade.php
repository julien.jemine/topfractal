@extends('layouts.master')

@section('title')
{{Lang::get('messages.welcome_dashboard')}}
@endsection

@section('content')

@include('includes.nav-bar')

<div class="container-fluid">

    @if( Session::has('error') )
    <div class="row margin-top-20">
        <div class="col-md-12">
            <div class="alert alert-danger margin-bottom-0">
                <button type="button" class="close fui-cross" data-dismiss="alert"></button>
                {{ Session::get('error') }}
            </div>
        </div><!-- /.col -->
    </div>
    @endif

    <div class="row custom-flex">
        <div class="col-md-9 col-sm-8">
            <h1><span class="fui-windows"></span> {{Lang::get('messages.Sites')}}</h1>
        </div><!-- /.col -->

        <div class="col-md-3 col-sm-4 text-right">
            <a href="{{ route('layouts', app()->getLocale()) }}" class="btn btn-lg btn-primary btn-embossed btn-wide margin-top-40"><span class="fui-plus"></span> {{Lang::get('messages.Create_New_Site')}}</a>
           <!--  <a href="{{ route('layouts', app()->getLocale()) }}" class="btn btn-lg btn-primary btn-embossed btn-wide margin-top-40"><span class="fui-plus"></span> Create New Template</a> -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    <hr class="dashed">

    <div class="row margin-bottom-30">
        @if (Auth::user()->type == 'admin')
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <select name="userDropDown" id="userDropDown" class="form-control select select-inverse btn-block mbl ">
                    <option value="">{{Lang::get('messages.Filter_By_User')}}</option>
                    <option value="All">{{Lang::get('messages.All')}}</option>
                    @foreach($users as $user)
                    <option value="{{ $user->first_name }}">{{ $user->email }}</option>
                    @endforeach
                </select>
            </div>
        </div><!-- /.col -->
        @endif

        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <select name="sortDropDown" id="sortDropDown" class="form-control select select-inverse select-block mbl" >
                    <option value="">{{Lang::get('messages.Sort_by')}}</option>
                    <option value="CreationDate">{{Lang::get('messages.Creation_date')}}</option>
                    <option value="LastUpdate">{{Lang::get('messages.Last_updated')}}</option>
                    <option value="NoOfPages">{{Lang::get('messages.Number_of_pages')}}</option>
                </select>
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->

    <div class="row">
        @if( isset($sites) && count( $sites ) > 0 )
        <div class="col-md-12">
            <?php //print_r($sites); ?>
            <div class="masonry-4 sites" id="sites">
                @foreach( $sites as $site )
                <?php //print_r($site); ?>
                <div class="site" data-name="{{ $site['siteData']['user']['first_name'] }} " data-pages="{{ $site['nrOfPages'] }}" data-created="{{ date('Y-m-d', strtotime($site['siteData']['created_at'])) }}" data-update="{{ date('Y-m-d', strtotime($site['siteData']['updated_at'])) }}" id="site_{{ $site['siteData']['id'] }}">
                    <div class="window">
                        <div class="top">
                            <div class="buttons clearfix">
                                <span class="left red"></span>
                                <span class="left yellow"></span>
                                <span class="left green"></span>
                            </div>
                            <b>{{ $site['siteData']['site_name'] }}</b>
                        </div><!-- /.top -->

                        <div class="viewport" data-href="#">
                            @if( $site['lastFrame'] != '' )
                            <iframe src="{{ route('getframe', ['frame_id' => $site['lastFrame']['id']]) }}" frameborder="0" scrolling="0" data-height="500" data-siteid="{{ $site['siteData']['id'] }}" data-href="#" class="iframeButton"></iframe>
                            @else
                            <a href="#" target="_blank" class="placeHolder">
                                <span>This site is empty</span>
                            </a>
                            @endif
                            <p>
                            <div class="siteLink mobile">
                            @if( $site['siteData']['ftp_published'] == 1 )
                                @if( $site['siteData']['remote_url'] != '' )
                                <span class="fui-link"></span> <a href="http:\\{{ $site['siteData']['remote_url'] }}" target="_blank">{{ $site['siteData']['remote_url'] }}</a><br/>
                                <p>{{Lang::get('messages.The_site_is_published_until')}} {{date('Y-m-d', strtotime($site['siteData']['publish_date']))}}</p>
                                @else
                                    {{Lang::get('messages.The_site_is_published_until')}} {{ date('Y-m-d', strtotime($site['siteData']['publish_date'])) }}
                                <!-- Site was published to {{ date('Y-m-d', strtotime($site['siteData']['publish_date'])) }} -->
                                @endif
                            @else
                            @if($site['siteData']['ftp_published'] == 2)
                                <span class="pull-left text-danger">
                                    <b>{{Lang::get('messages.Site_has_not_been_published_Payment_expired')}}</b>
                                </span> &nbsp;&nbsp;
                                @else
                                <span class="pull-left text-danger">
                                <b>{{Lang::get('messages.Site_has_not_been_published')}}</b>
                                </span> &nbsp;&nbsp;
                                @endif
                            

                            @endif
                            </div>
                        </p>
                            <button class="show-more mobile"> <a onclick="showMore({{ $site['siteData']['id'] }} )"/>
                                <svg class="arrow-to-right rotate-active" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                            width="451.846px" height="451.847px" viewBox="0 0 451.846 451.847" style="enable-background:new 0 0 451.846 451.847;"
	                            xml:space="preserve">
                            <g>
	                        <path fill="#34495e" d="M345.441,248.292L151.154,442.573c-12.359,12.365-32.397,12.365-44.75,0c-12.354-12.354-12.354-32.391,0-44.744
	                        	L278.318,225.92L106.409,54.017c-12.354-12.359-12.354-32.394,0-44.748c12.354-12.359,32.391-12.359,44.75,0l194.287,194.284
	                        	c6.177,6.18,9.262,14.271,9.262,22.366C354.708,234.018,351.617,242.115,345.441,248.292z"/>
                            </g>
                            </svg>
                            </button>
                        </div><!-- /.viewport -->

                        <div class="bottom">
                        
                        </div><!-- /.bottom -->
                        <button class="show-more mobile-none"> <a onclick="showMore({{ $site['siteData']['id'] }} )"/>
                                <svg class="arrow-to-right rotate-active" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	                            width="451.846px" height="451.847px" viewBox="0 0 451.846 451.847" style="enable-background:new 0 0 451.846 451.847;"
	                            xml:space="preserve">
                            <g>
	                        <path fill="#34495e" d="M345.441,248.292L151.154,442.573c-12.359,12.365-32.397,12.365-44.75,0c-12.354-12.354-12.354-32.391,0-44.744
	                        	L278.318,225.92L106.409,54.017c-12.354-12.359-12.354-32.394,0-44.748c12.354-12.359,32.391-12.359,44.75,0l194.287,194.284
	                        	c6.177,6.18,9.262,14.271,9.262,22.366C354.708,234.018,351.617,242.115,345.441,248.292z"/>
                            </g>
                            </svg>
                            </button>
                    </div><!-- /.window -->

                    <div class="siteDetails" id="siteDet_{{ $site['siteData']['id'] }}" style="display: none;">
                        <p>
                            {{Lang::get('messages.Owner')}}<b>{{ $site['siteData']['user']['first_name'] }}</b>, {{ $site['nrOfPages'] }} {{Lang::get('messages.Pages')}}<br>
                            {{Lang::get('messages.Created_on')}}: <b>{{ date('Y-m-d', strtotime($site['siteData']['created_at'])) }}</b><br>
                            {{Lang::get('messages.Last_edited_on')}}: <b>{{ date('Y-m-d', strtotime($site['siteData']['updated_at'])) }}</b>
                        </p>

                        <p class="siteLink mobile-none">
                            @if( $site['siteData']['ftp_published'] == 1 )
                                @if( $site['siteData']['remote_url'] != '' )
                                <span class="fui-link"></span> <a href="http:\\{{ $site['siteData']['remote_url'] }}" target="_blank">{{ $site['siteData']['remote_url'] }}</a><br />
                                <p class="mobile-none">{{Lang::get('messages.The_site_is_published_until')}} {{date('Y-m-d', strtotime($site['siteData']['publish_date']))}}</p>
                                @else
                                    {{Lang::get('messages.The_site_is_published_until')}} {{ date('Y-m-d', strtotime($site['siteData']['publish_date'])) }}
                                <!-- Site was published to {{ date('Y-m-d', strtotime($site['siteData']['publish_date'])) }} -->
                                @endif
                            @else
                            @if($site['siteData']['ftp_published'] == 2)
                                <span class="pull-left text-danger">
                                    <b>{{Lang::get('messages.Site_has_not_been_published_Payment_expired')}}</b>
                                </span> &nbsp;&nbsp;
                                @else
                                <span class="pull-left text-danger">
                                <b>{{Lang::get('messages.Site_has_not_been_published')}}</b>
                                </span> &nbsp;&nbsp;
                                @endif
                            

                            @endif
                        </p>

                        <hr class="dashed light">

                        <div class="clearfix">
                            <a href="{{ route('site', ['locale'=>app()->getLocale(),'site_id' => $site['siteData']['id']]) }}" class="btn btn-primary btn-embossed btn-block"><span class="fui-new"></span> {{Lang::get('messages.Edit_This_Site')}}</a>
                            <a href="#" class="btn btn-info btn-embossed btn-block btn-half pull-left btn-sm siteSettingsModalButton first" data-siteid="{{ $site['siteData']['id'] }}"><span class="fui-gear"></span> {{Lang::get('messages.Pay')}}</a>
                            <a href="#deleteSiteModal" class="btn btn-danger btn-embossed btn-block btn-half pull-left deleteSiteButton btn-sm second" id="deleteSiteButton" data-siteid="{{ $site['siteData']['id'] }}"><span class="fui-trash"></span> {{Lang::get('messages.Delete')}}</a>
                        </div>
                    </div><!-- /.siteDetails -->
                </div><!-- /.site -->
                @endforeach
            </div><!-- /.masonry -->
        </div><!-- /.col -->
        @else
        <div class="col-md-6 col-md-offset-3">
            <div class="alert alert-info" style="margin-top: 30px">
                <button type="button" class="close fui-cross" data-dismiss="alert"></button>
                <h2>{{Lang::get('messages.fuiCross_Hello')}}</h2>
                <p>
                    {{Lang::get('messages.fuiCross_Text')}}
                </p>
                <br><br>
                <a href="{{ route('site-create', ['locale' => app()->getLocale(), 'page' => '83']) }}" class="btn btn-primary btn-lg btn-wide">{{Lang::get('messages.fuiCross_submit')}}</a>
                <a href="#" class="btn btn-default btn-lg btn-wide" data-dismiss="alert">{{Lang::get('messages.fuiCross_dismiss')}}</a>
            </div>
        </div><!-- ./col -->
        @endif

    </div><!-- /.row -->

</div><!-- /.container -->

<!-- modals -->

@include('includes.modal-sitesettings')

@include('includes.modal-account')

@include('includes.modal-deletesite')

<!-- /modals -->

<!-- Load JS here for greater performance -->
<script src="{{ URL::to('src/js/vendor/jquery.min.js') }}"></script>
<script src="{{ URL::to('src/js/vendor/jquery-ui.min.js') }}"></script>
<script src="{{ URL::to('src/js/vendor/flat-ui-pro.min.js') }}"></script>
<script src="{{ URL::to('src/js/vendor/jquery.zoomer.js') }}"></script>
<script src="{{ URL::to('src/js/build/sites.js') }}"></script>
<script src="{{ URL::to('src/js/bower_components/card-info/dist/card-info.min.js') }}"></script>
<script src="{{ URL::to('src/js/sweetalert/dist/sweetalert2.min.js') }}"></script>
<script src="https://snipp.ru/cdn/maskedinput/jquery.maskedinput.min.js"></script>
<script src=""></script>
<script>
    $(".viewport").on('click', function(el){
        
        event.preventDefault();
        $(location).attr('href','http://' + $(this).attr('data-href'));
    })

    $('.siteSettingsModalButton ').on('click', function(){
        $('#siteSettings').attr('data-site', $(this).attr('data-siteid'));
    })
</script>
<script>
    $('#cc-number').on('keyup', function(){
        var num = parseInt($('#cc-number').val().replace(/\D+/g,""));
        if(num.toString().length >= 4) {
            var cardInfo = new CardInfo(num.toString());
            if(cardInfo.brandLogo) {
                $('#card-logo').attr('src', $('#env-url').val() + '/src/js' +cardInfo.brandLogo)
                $('#card-logo').width('50');
                $('#card-logo').height('50');
            } else {
                $('#card-logo').attr('src', 'https://bootstraptema.ru/snippets/form/2016/form-card/card.png')
            }
        } else {
            $('#card-logo').attr('src', 'https://bootstraptema.ru/snippets/form/2016/form-card/card.png')
        }
        
    })
    

    $('#cc-number').mask('9999 9999 9999 9999');
    $('#cc-date').mask('99/99');
    $('#cc-ccv').mask('999');
    

    $('#payLiqpay').on('click', function(e){
       
        e.preventDefault();
        console.log($(this).attr);
                $.ajax({
                    url: "/siteAjaxUpdate",
                    type: 'post',
                    data: {
                        'siteID': $('#siteSettings').attr('data-site'),
                        'cc-number': $('#cc-number').val(),
                        'cc-date': $('#cc-date').val(),
                        'cc-ccv': $('#cc-ccv').val(),
                        'locale': $('#locale').val(),
                    }
                }).then((res) => {
                    if(res.status == 'ok') {
                        $.ajax({
                        url: "/restart",
                        type: 'get',
                        });
                        Swal.fire({
                          position: 'top-end',
                          icon: 'success',
                          title: res.message,
                          showConfirmButton: false,
                          timer: 3500
                        })
                        $('#payLiqpay').hide();
                    } else {
                        Swal.fire({
                          'text': res.message,
                          'icon': 'error',
                        }) 
                    }
                })
       })
</script>

<script>
//    let siteIdCustom = $('#site_{{ $site['siteData']['id'] }}');
//    const siteDetails = $('.siteDetails');
    const showBtn = $('.show-more');
    
    $('.siteDetails').css('display', 'none');
    $(showBtn).css('background-color', 'transparent');
    $(showBtn).css('border', 'none');
    
//    showBtn.on('click', function() {
//        $(siteIdCustom).each(function(e) {
//            $(this).children(siteDetails).toggle('display');
//        })
//    });
function showMore(id) {
    let siteDec = document.getElementById("siteDet_"+id);
    
    if(siteDec.classList.contains("is-active-custom")) {
        siteDec.style.display = "none";
        siteDec.classList.remove("is-active-custom");
    } else {
        siteDec.style.display = "block";
        siteDec.classList.add("is-active-custom");
    }
}
</script>

<!--[if lt IE 10]>
<script>
$(function(){
	var msnry = new Masonry( '#sites', {
    	// options
    	itemSelector: '.site',
    	"gutter": 20
    });

})
</script>
<![endif]-->
@endsection
