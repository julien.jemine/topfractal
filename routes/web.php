<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/redirect', 'SocialAuthFacebookController@redirect')->name('facebook.redirect');
Route::get('/restart', 'SiteController@restartApach');
Route::get('/callback', 'SocialAuthFacebookController@callback');

Route::get('auth/google', 'Auth\LoginController@redirectToGoogle');
Route::get('auth/callback/google', 'Auth\LoginController@handleGoogleCallback');

Route::get('/', function () {
    return redirect(app()->getLocale() . '/');
})->name('home');

Route::get('/dashboard', function () {
    return redirect(app()->getLocale() . '/dashboard');
});

Route::get('/newDomain', [
    'uses' => 'SiteController@newDomain',
    'as' => 'newDomain',
    'middleware' => 'auth'
]);

Route::get('/siteAjax/{site_id}', [
    'uses' => 'SiteController@getSiteAjax',
    'as' => 'siteAjax',
    'middleware' => 'auth'
]);

// Site and Page settings
Route::post('/siteAjaxUpdate', [
    'uses' => 'SiteController@postAjaxUpdate',
    'as' => 'siteAjaxUpdate',
    'middleware' => 'auth'
]);


Route::post('/site/save', [
    'uses' => 'SiteController@postSave',
    'as' => 'site-save',
    'middleware' => 'auth'
])->name('site-save');

// Revision section route
Route::get('/site/getRevisions/{site_id}/{page}', [
    'uses' => 'SiteController@getRevisions',
    'as' => 'getRevisions',
    'middleware' => 'auth'
]);

Route::group([
    'prefix' => '{locale}',
    'where' => ['locale' => '[a-zA-Z]{2}'],
    'middleware' => 'setlocale'
], function () {

    Auth::routes();

    Route::get('/', function () {
        return view('pages.home');
    })->name('home.page');

    Route::get('/oferta', function () {
        return view('pages.oferta');
    })->name('oferta');

    Route::get('/polityka-konfidentsijnosti', function () {
        return view('pages.polityka');
    })->name('polityka.konfidentsijnosti');

    Route::get('/layouts', function () {
        return view('pages.layouts');
    })->name('layouts');

    // Dashboard section
    Route::get('/dashboard', [
        'uses' => 'SiteController@getDashboard',
        'as' => 'dashboard',
        'middleware' => 'auth'
    ]);

    

    // Site section route
    Route::get('/site-create/{page}', [
        'uses' => 'SiteController@SiteCreate',
        'as' => 'site-create',
        'middleware' => 'auth'
    ]);

    Route::post('/site-create-form', [
        'uses' => 'SiteController@getSiteCreate',
        'as' => 'site-create-form',
        'middleware' => 'auth'
    ]);

    Route::get('/site/{site_id}', [
        'uses' => 'SiteController@getSite',
        'as' => 'site',
        'middleware' => 'auth'
    ]);


    Route::post('/site/tsave', [
        'uses' => 'SiteController@postTSave',
        'as' => 'site-tsave',
        'middleware' => 'auth'
    ])->name('site-tsave');;

    Route::get('/siteData', [
        'uses' => 'SiteController@getSiteData',
        'as' => 'siteData',
        'middleware' => 'auth'
    ]);

    

    Route::get('/deleterevision/{site_id}/{datetime}/{page}', [
        'uses' => 'SiteController@getRevisionDelete',
        'as' => 'revision.delete',
        'middleware' => 'auth'
    ]);

    Route::get('/restorerevision/{site_id}/{datetime}/{page}', [
        'uses' => 'SiteController@getRevisionRestore',
        'as' => 'revision.restore',
        'middleware' => 'auth'
    ]);

    // Publish and export section
    Route::post('/site/export', [
        'uses' => 'SiteController@postExport',
        'as' => 'site.export',
        'middleware' => 'auth'
    ]);

    Route::post('/site/publish/{type?}', [
        'uses' => 'SiteController@postPublish',
        'as' => 'site.publish',
        'middleware' => 'auth'
    ]);

    // FTP section route
    Route::post('/site/connect', [
        'uses' => 'SiteController@postFTPConnect',
        'as' => 'ftp.connect',
        'middleware' => 'auth'
    ]);

    Route::post('/site/ftptest', [
        'uses' => 'SiteController@postFTPTest',
        'as' => 'ftp.test',
        'middleware' => 'auth'
    ]);

    Route::get('/test', [
        'uses' => 'SiteController@getTest',
        'as' => 'site.test',
        'middleware' => 'auth'
    ]);

    // Live preview route
    

    Route::post('/updatePageData', [
        'uses' => 'SiteController@postUpdatePageData',
        'as' => 'updatePageData',
        'middleware' => 'auth'
    ]);

    // User section route
    Route::get('/users', [
        'uses' => 'UserController@getUserList',
        'as' => 'users',
        'middleware' => 'auth'
    ]);

    Route::get('/benefits', [
        'uses' => 'SiteController@Benefits',
        'as' => 'benefits',
        'middleware' => 'auth'
    ]);

    Route::post('/benefits', [
        'uses' => 'SiteController@addBenefits',
        'as' => 'benefits.add',
        'middleware' => 'auth'
    ]);

    Route::get('/benefits-delete/{benefit}', [
        'uses' => 'SiteController@deleteBenefits',
        'as' => 'benefits-delete',
        'middleware' => 'auth'
    ]);

    Route::get('/domains', [
        'uses' => 'SiteController@getDomain',
        'as' => 'domains',
        'middleware' => 'auth'
    ]);

    Route::get('/services', [
        'uses' => 'SiteController@Services',
        'as' => 'services',
        'middleware' => 'auth'
    ]);

    Route::get('/services-benefits', [
        'uses' => 'SiteController@ServicesBenefits',
        'as' => 'services-benefits',
        'middleware' => 'auth'
    ]);

    Route::get('/services-parent/{service}', [
        'uses' => 'SiteController@parentServices',
        'as' => 'services-parent',
        'middleware' => 'auth'
    ]);

    Route::post('/services', [
        'uses' => 'SiteController@addServices',
        'as' => 'services.add',
        'middleware' => 'auth'
    ]);

    Route::get('/services-delete/{service}', [
        'uses' => 'SiteController@deleteServices',
        'as' => 'services-delete',
        'middleware' => 'auth'
    ]);

    Route::post('/user-create', [
        'uses' => 'UserController@postUserCreate',
        'as' => 'user-create',
        'middleware' => 'auth'
    ]);

    Route::post('/user-update', [
        'uses' => 'UserController@postUserUpdate',
        'as' => 'user-update',
        'middleware' => 'auth'
    ]);

    Route::get('/user-delete/{user_id}', [
        'uses' => 'UserController@getUserDelete',
        'as' => 'user-delete',
        'middleware' => 'auth'
    ]);

    Route::get('/user-enable-disable/{user_id}', [
        'uses' => 'UserController@getUserEnableDisable',
        'as' => 'user-enable-disable',
        'middleware' => 'auth'
    ]);

    Route::post('/user/uaccount', [
        'uses' => 'UserController@postUAccount',
        'as' => 'user.uaccount',
        'middleware' => 'auth'
    ]);

    Route::post('/user/ulogin', [
        'uses' => 'UserController@postULogin',
        'as' => 'user.ulogin',
        'middleware' => 'auth'
    ]);

    // Settings section route
    Route::get('/settings', [
        'uses' => 'SettingController@getSetting',
        'as' => 'settings',
        'middleware' => 'auth'
    ]);

    Route::post('/edit-settings', [
        'uses' => 'SettingController@postSetting',
        'as' => 'edit-settings',
        'middleware' => 'auth'
    ]);

    // Image Library section route
    Route::get('/assets', [
        'uses' => 'AssetController@getAsset',
        'as' => 'assets',
        'middleware' => 'auth'
    ]);

    Route::post('/upload-image', [
        'uses' => 'AssetController@uploadImage',
        'as' => 'upload.image',
        'middleware' => 'auth'
    ]);

    
});

Route::post('/site/live/preview', [
    'uses' => 'SiteController@postLivePreview',
    'as' => 'live.preview',
    'middleware' => 'auth'
]);

Route::get('/site/getframe/{frame_id}', [
    'uses' => 'SiteController@getFrame',
    'as' => 'getframe',
    'middleware' => 'auth'
]);

Route::get('/site/rpreview/{site_id}/{datetime}/{page}', [
    'uses' => 'SiteController@getRevisionPreview',
    'as' => 'revision.preview',
    'middleware' => 'auth'
]);

Route::get('/new-domain', [
    'uses' => 'SiteController@newDomain',
    'as' => 'newDomain',
    'middleware' => 'auth'
]);

  Route::get('/site/trash/{site_id}', [
        'uses' => 'SiteController@getTrash',
        'as' => 'site.trash',
        'middleware' => 'auth'
    ]);

Route::post('/image-upload-ajax', [
    'uses' => 'AssetController@imageUploadAjax',
    'as' => 'image.upload.ajax',
    'middleware' => 'auth'
]);

Route::post('/delImage', [
    'uses' => 'AssetController@delImage',
    'as' => 'delImage',
    'middleware' => 'auth'
]);